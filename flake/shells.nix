{
  perSystem = {pkgs, ...}: {
    devShells = {
      "default" = pkgs.mkShellNoCC {
        packages = [
          # JavaScript
          pkgs.nodejs-slim
          (
            pkgs.pnpm.override {withNode = false;}
          )

          # Nix
          pkgs.deadnix
          pkgs.nil
          pkgs.statix
        ];
      };
    };
  };
}
